from django.test import TestCase, Client
from django.urls import resolve, reverse
from about import views
from .views import about

class AboutTestCase(TestCase):
    def test_about_url_exists(self):
        response = self.client.get(reverse('about:about'))
        self.assertEqual(response.status_code, 200)

    def test_about_using_about_func(self):
        found = resolve(reverse('about:about'))
        self.assertEqual(found.func, views.about)

    def test_about_using_about_template(self):
        response = self.client.get(reverse('about:about'))
        self.assertTemplateUsed(response, 'about.html')

    def test_my_name_in_about(self):
        text = "Audrey Annisa Datau"
        response = self.client.get(reverse('about:about'))
        content = response.content.decode('utf8')
        self.assertIn(text, content)



    
