from django.test import TestCase, Client
from django.urls import resolve, reverse
from books import views
from .views import books

class BooksTestCase(TestCase):
    def test_books_url_exists(self):
        response = self.client.get(reverse('books:books'))
        self.assertEqual(response.status_code, 200)

    def test_books_using_about_func(self):
        found = resolve(reverse('books:books'))
        self.assertEqual(found.func, views.books)

    def test_books_using_about_template(self):
        response = self.client.get(reverse('books:books'))
        self.assertTemplateUsed(response, 'books.html')


