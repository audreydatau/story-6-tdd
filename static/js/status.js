$(".accordion_tab").click(function(){
    $(".accordion_tab").each(function(){
      $(this).parent().removeClass("active");
      $(this).removeClass("active");
    });
    $(this).parent().addClass("active");
    $(this).addClass("active");
});


function changeThemeGreen(){
    document.body.style.backgroundColor = '#d3f6f3';
    document.body.style.color = 'black';
    document.getElementsByTagName('nav')[0].style.backgroundColor ="#cbf1f5";
    document.getElementById("text-nav-status").style.color ="black";
    document.getElementById("text-nav-about").style.color = "black";
    document.getElementById("text-nav-theme").style.color = "black";
    document.getElementById("text-nav-books").style.color = "black";
    if(document.title=="status"){
        var judultabel = document.getElementsByTagName('th');
        for(var i=0;i<judultabel.length;i++){
            judultabel[i].style.color = "black";
        }
        var isitabel = document.getElementsByTagName('td');
        for(var i=0;i<isitabel.length;i++){
            isitabel[i].style.color = "black";
        }
        document.getElementsByTagName('button')[0].style.background ="#007BFF";
    }
    if(document.title=="about"){
        var judulaccordion = document.getElementsByClassName('accordion_tab');
        for(var i=0;i<judulaccordion.length;i++){
            judulaccordion[i].style.color = "black";
            judulaccordion[i].style.backgroundColor ="white";
        }
        var judulitem = document.getElementsByTagName('p');
        for(var i=1;i<judulitem.length;i+=2){
            judulitem[i].style.color = "#6adda2";
        }
    }
    if(document.title=="books"){
        document.getElementsByTagName('button')[0].style.background ="#007BFF";
        var paragraf = document.getElementsByTagName('p');
        for(var i=0;i<paragraf.length;i++){
            paragraf[i].style.color = "black";
        }
        var judul = document.getElementsByTagName('td');
        for(var i=0;i<judul.length;i++){
            judul[i].style.color = "white";
        }
    }
    if(document.title=="Sign In"){
        document.getElementsByClassName('user_card')[0].style.background="#a6e3e9";
    }
}

function changeThemeMonochrome(){
    document.body.style.backgroundColor = '#232931';
    document.body.style.color = 'white';
    document.getElementsByTagName('nav')[0].style.backgroundColor ="#393e46";
    document.getElementById("text-nav-status").style.color = "white";
    document.getElementById("text-nav-about").style.color = "white";
    document.getElementById("text-nav-theme").style.color = "white";
    document.getElementById("text-nav-books").style.color = "white";
    if(document.title=="status"){
        var judultabel = document.getElementsByTagName('th');
        for(var i=0;i<judultabel.length;i++){
            judultabel[i].style.color = "white";
        }
        var isitabel = document.getElementsByTagName('td');
        for(var i=0;i<isitabel.length;i++){
            isitabel[i].style.color = "white";
        }
        document.getElementsByTagName('button')[0].style.background ="grey";
    }
    if(document.title=="about"){
        var judulaccordion = document.getElementsByClassName('accordion_tab');
        for(var i=0;i<judulaccordion.length;i++){
            judulaccordion[i].style.color = "white";
            judulaccordion[i].style.backgroundColor ="#393e46";
        }
        var judulitem = document.getElementsByTagName('p');
        for(var i=1;i<judulitem.length;i+=2){
            judulitem[i].style.color = "black";
        }
    }
    if(document.title=="books"){
        document.getElementsByTagName('button')[0].style.background ="grey";
        var judul = document.getElementsByTagName('td');
        var paragraf = document.getElementsByTagName('p');
        for(var i=0;i<paragraf.length;i++){
            paragraf[i].style.color = "white";
        }
        for(var i=0;i<judul.length;i++){
            judul[i].style.color = "white";
        }
    }
    if(document.title=="Sign In"){
        document.getElementsByClassName('user_card')[0].style.background="grey";
    }
}

