[![pipeline status](https://gitlab.com/audreydatau/story-6-tdd/badges/master/pipeline.svg)](https://gitlab.com/audreydatau/story-6-tdd/commits/master)
[![coverage report](https://gitlab.com/audreydatau/story-6-tdd/badges/master/coverage.svg)](https://gitlab.com/audreydatau/story-6-tdd/commits/master)

Test-Driven Development (TDD).

## [my website](https://testingaudrey.herokuapp.com)
